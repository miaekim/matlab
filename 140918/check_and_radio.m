function varargout = check_and_radio(varargin)
% CHECK_AND_RADIO M-file for check_and_radio.fig
%      CHECK_AND_RADIO, by itself, creates a new CHECK_AND_RADIO or raises the existing
%      singleton*.
%
%      H = CHECK_AND_RADIO returns the handle to a new CHECK_AND_RADIO or the handle to
%      the existing singleton*.
%
%      CHECK_AND_RADIO('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CHECK_AND_RADIO.M with the given input arguments.
%
%      CHECK_AND_RADIO('Property','Value',...) creates a new CHECK_AND_RADIO or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before check_and_radio_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to check_and_radio_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help check_and_radio

% Last Modified by GUIDE v2.5 18-Sep-2014 10:58:04

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @check_and_radio_OpeningFcn, ...
                   'gui_OutputFcn',  @check_and_radio_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before check_and_radio is made visible.
function check_and_radio_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to check_and_radio (see VARARGIN)

% Choose default command line output for check_and_radio
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
set(handles.pushbutton1, 'visible', 'off');
set(handles.pushbutton2, 'enable', 'off');
set(handles.pushbutton3, 'enable', 'off');
set(handles.pushbutton4, 'enable', 'off');
    
% UIWAIT makes check_and_radio wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = check_and_radio_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1
if (get(hObject, 'Value') == get(hObject, 'Max'))
    set(handles.pushbutton2, 'enable', 'on');
    set(handles.pushbutton3, 'enable', 'off');
    set(handles.pushbutton4, 'enable', 'off');
else
    set(handles.pushbutton3, 'enable', 'off');
end


% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton2
if (get(hObject, 'Value') == get(hObject, 'Max'))
    set(handles.pushbutton2, 'enable', 'off');
    set(handles.pushbutton3, 'enable', 'on');
    set(handles.pushbutton4, 'enable', 'off');
else
    set(handles.pushbutton3, 'enable', 'off');
end

% --- Executes on button press in radiobutton3.
function radiobutton3_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton3


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1

if (get(hObject, 'Value') == get(hObject, 'Max'))
    set(handles.pushbutton1, 'visible', 'on');
else
    set(handles.pushbutton1, 'visible', 'off');
end


