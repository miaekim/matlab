공학 컴퓨터 프로그래밍 
=====================

This is a set of basic MATLAB source code.
This project uses MATLAB 2007.

```MATLAB
clear a; % clear variable 'a'
clear all; % clear all variable before *.m script
close all; % close all graphs
fclose all; % close resource files such as text, image file
clc; 
a = 1;
b = a + 2;
c = [1:2:100]
d = [1:2:100]
e = [a+b]

```


These are matlab short cuts.
 
 1. Align text, Ctrl + I
 2. Comment, Ctrl + R
 3. Remove comments, Ctrl + T


